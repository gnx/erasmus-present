---
theme: uncover, enable-all-auto-scaling
backgroundColor: #ddffcc
color: #206020
headingDivider: 5
paginate: true
size: 16:9
inlineSVG: true
---

# Team from Gabrovo, Bulgaria
![bg right:60%](pmg-aerial.png)
Science and Maths High School "Akad. Ivan Gyuzelev"

Europass teacher training course
Classroom Management Solutions for Teachers
Dublin, Ireland
3-8 April 2023

# Bulgaria
![bg right:70%](bulgaria-map.svg)
![bg right](sofia.jpg)
Population:
6 500 000

# Gabrovo
![bg right:70%](gabrovo-map.png)
![bg right](gabrovo.jpg)
Population:
52 000

# Science and Maths High School
![bg right:70%](pmg-map.png)
![bg right](pmg-aerial.png)
Students: ? 600 ?
Teachers: ? 45 ?

#
<video controls>
    <source src="horo.webm" type="video/webm">
    Download the
    <a href="horo.webm">WEBM</a>
</video>

# Team from Gabrovo, Bulgaria
???

- Gabriela Stoyanova
- Hristo Hristov
- Maria Popova
- Nikola Atanasov
- Rosita Paneva
- Vanesa Peneva

???